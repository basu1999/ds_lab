#include <stdio.h>
#include <stdlib.h>

//Define node
struct node
{

    int val;
    struct node * next;
};

//Head pointer
struct node * head=NULL;

//Create node
struct node * create_node(int val)
{
    struct node * temp=malloc(sizeof(struct node));
    temp->val=val;
    temp->next=NULL;
    return temp;
}

void create_list()
{
    if(head !=NULL)
        printf("List is already initialized!\n");
    else
    {
        int val;
        printf("Enter value for the first node: ");
        scanf("%d",&val);
        struct node *temp=create_node(val);
        head=temp;
    }
}
    


//Insert at Begining
void insert_beg(int val)
{
    struct node * temp=create_node(val);
    temp->next=head;
    head=temp;
}

//Insert
void insert(int val, struct node * prev_node)
{
    struct node * temp=create_node(val);
    temp->next=prev_node->next;
    prev_node->next=temp;
}
//Delete
void delete(struct node * prev_node)
{
    struct node * temp=prev_node ->next;
    prev_node->next=temp->next;
    free(temp);
}


//Length
int length()
{
    int count=0;
    struct node * temp=head;
    while(temp)
    {
        ++count;
        temp=temp->next;
    }
    return count;
}

/*//Find

int find_val(struct node * head,int val)
{
    struct node * temp=head;
    while(temp)
    {
        if (temp->val==val)
            return 1;
        temp=temp->next;
    }
    return 0;
}
*/



//Print
void print()
{
    printf("The linked list: ");
    struct node * temp=head;
    while(temp)
    {
        printf("%d -> ",temp->val);
       temp=temp->next;
    }
    printf("\n");
} 




int main(int argc,char **argv)
{
    struct node * temp;
    int choice;
    int val,pos,len;
    while(1)
    {
        printf("------------Menu-------------\n\n");
        printf("1.Create List\n");
        printf("2.Insert at beginning\n");
        printf("3.Insert after a node\n");
        printf("4.Delete after a node\n");
        printf("5.Print length\n");
        printf("6.Print the list\n");
        printf("Enter a choice: ");
        scanf("%d",&choice);

        switch(choice)
        {
            case 1:
                    create_list();
                    break;
            case 2:
                    printf("Enter the value for first node: ");
                    scanf("%d",&val);
                    insert_beg(val);
                    break;
            case 3:
                    printf("Node at which position?");
                    scanf("%d",&pos);
                    temp=head;
                    for(int i=0;i<pos-1;i++)
                        temp=temp->next;
                    printf("Enter the value of the new node: ");
                    scanf("%d",&val);
                    insert(val,temp);
                    break;
            case 4:
                    printf("Node at which position?");
                    scanf("%d",&pos);
                    temp=head;
                    for(int i=0;i<pos-1;i++)
                        temp=temp->next;
                    delete(temp);
                    break;
             
            case 5:
                    len=length();
                    printf("The length of the list= %d\n",len);
                    break;
            case 6:
                    print();
                    break;
            default:
                    printf("Wrong choice!\n");
                    break;
        }
    }



                        






    return 0;
 
}


