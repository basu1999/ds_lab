/* Represents a directed graph as an adjacency list */
#include <stdio.h>
#include <stdlib.h>

//define node
struct node {
    int index;
    struct node * next;
};

//define Graph
struct Graph {
    int num_nodes;
    struct node **adjList;

};

//create a node
struct node * CreateNode(int s)
{
    struct node * temp=malloc(sizeof(struct node));
    temp->index=s;
    temp->next=NULL;
    return temp;
}


//create a graph
struct Graph * CreateGraph(int num)
{
    struct Graph * temp=malloc(sizeof(struct Graph));
    temp->num_nodes=num;
    temp->adjList=malloc(num*sizeof(struct node));
    for(int i=0;i<num;i++)
        temp->adjList[i]=NULL;
    return temp;
}

//Add edge
void AddEdge(struct Graph * graph, int source, int dest)
{
    struct node * temp=CreateNode(dest);
    temp->index=dest;
    temp->next=graph ->adjList[source]; //make temp the first entry
    graph->adjList[source]=temp;

}


//print the graph
void PrintGraph(struct Graph * graph)
{
    struct node * temp=malloc(sizeof(struct node));
    for(int i=0;i<graph->num_nodes;i++)
    {
        printf("%d ",i);
        temp=graph->adjList[i];
        while(temp)
        {
            printf("-> %d ",temp->index);
            temp=temp->next;
        }
        printf("\n");
    }

}

//Main function

int main(int argc,char **argv)
{
   FILE *file;
   int num,source,dest;
   file=fopen("graph.txt","r");

   if(file==NULL)
   {
       fprintf(stderr,"No such file!\n");
       exit(1);
   }

   fscanf(file,"%d",&num);// number of edges in the graph

   struct Graph *graph=CreateGraph(num);

   while(fscanf(file,"%d %d",&source,&dest)==2)
   {
       AddEdge(graph,source,dest);
   }
   fclose(file);
   PrintGraph(graph);


}




